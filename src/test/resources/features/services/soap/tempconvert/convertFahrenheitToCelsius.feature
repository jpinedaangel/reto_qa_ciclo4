
Feature: Conversor de temperatura de grados Fahrenheit a grados Celsius
  Yo como usuario del sistema conversor de temperatura
  necesito realizar conversiones de temperatura
  para conocer la equivalencia de grados Fahrenheit a grados Celsius


  Background: Acceso al aplicativo
    Given que el usuario tiene acceso al aplicativo conversor de temperatura

  @SuccessfulTemperatureConversion
  Scenario:Conversión de la temperatura normal del cuerpo de grados Fahrenheit a grados Celsius
    When  el usuario realiza una conversión de temperatura de 98.6 grados Fahrenheit a grados Celsius
    Then  obtiene 37 grados Celsius como resultado de la conversión

  @FailedTemperatureConversion
  Scenario: Conversión fallida de temperatura de grados Fahrenheit a grados Celsius
    When el usuario intenta realizar una conversión de temperatura a grados Celsius con un dato de tipo string "cincuenta" no válido
    Then obtiene como respuesta un mensaje de "Error"