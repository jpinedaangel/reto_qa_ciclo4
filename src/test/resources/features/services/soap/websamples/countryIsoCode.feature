Feature: Buscar el código ISO de un país
  AS
    usuario del sistema
  I WANT TO
    buscar el código ISO de un país
  SO THAT
    puedo realizar búscadas con el código ISO.

  Background:
    Given un usuario desea conocer el código ISO de un país

  Scenario: Consultar el código ISO de un país
    When el usuario envía el nombre de "Colombia"
    Then el usuario obtiene el código ISO "CO"

  Scenario: Consultar el código ISO de un país que no existe
    When el usuario envía el nombre de un país que no existe "Colonia"
    Then el usuario obtiene el mensaje "No country found by that name"