Feature: Busqueda de nombres de moneda
  Como usuario de un servicio de busquda de monedas de paises
  necesito validar que la funcionalidad busqueda por código de pais para el llenado
  de datos que se requieren para comprar productos internacionales

  @currencyNameGod
  Scenario: busqueda nombre moneda
    Given que el usuario quiera buscar el nombre de la moneda
    When el usuario hace la petición de busqueda de el codigo "USD"
    Then el ususario debería obtener como moneda "Dollars"

  @curremcyNameBad
  Scenario: busqueda incorrecta moneda
    Given que el usuario quiera buscar por nombre no valido
    When el usuario hace la petición de busqueda del codigo "Co"
    Then el ususario debería obtener como resultado "Currency code NOT found"