Feature: Búsqueda de bandera de pais

  Como usuario del sistema quiero verificar el funcionamiento del servicio
  de busqueda de banderas de paises suministrando su código ISO

  Scenario: Búsqueda correcta de bandera de pais según su codigo ISO

    Given que el usuario quiere buscar la bandera del país con código ISO "CO"
    When el usuario realiza la petición y confirma la acción
    Then el usuario debería poder visualizar la url de la bandera del pais "http://www.oorsprong.org/WebSamples.CountryInfo/Flags/Colombia.jpg"

  Scenario: Búsqueda incorrecta de bandera de pais según su codigo ISO

    Given que el usuario quiere hacer la busqueda del país con código ISO "co"
    When el usuario inserta los datos de búsqueda y envía la solicitud
    Then el usuario vera el mensaje "Country not found in the database"