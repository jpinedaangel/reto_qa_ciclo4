Feature: Búsqueda de nombres de países

  Como usuario del sistema necesito verificar el funcionamiento de la búsqueda de un país por su código ISO

  Scenario: Búsqueda de un país según su codigo ISO

    Given que el usuario quiere buscar el país con código ISO "CA"
    When el usuario hace la petición y confirma la acción
    Then el usuario debería ver el nombre del país "Canada"

  Scenario: Búsqueda incorrecta de un país según su codigo ISO

    Given que el usuario desea buscar el país con código ISO "ca"
    When el usuario introduce los datos de búsqueda y envía la nueva solicitud
    Then el usuario debería ver el mensaje "Country not found in the database"