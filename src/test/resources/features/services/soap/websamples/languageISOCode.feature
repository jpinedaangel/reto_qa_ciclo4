Feature: ISO Code de lenguaje
  Yo como Usuario  de un servicio de búsqueda   del ISO Code del Lenguaje de un pais
  necesito validar que la funcionalidad búsqueda  por nombre del lenguajep ara el llenado
  de datos que se requieren para validar el ISO Code del Lenguaje.

  @searchLenguaje
  Scenario: búsqueda  de el ISO Code del Lenguaje
    Given que el usuario quiera buscar el nombre del lenguaje "Spanish"
    When el usuario hace la petición de busqueda el ISO Code del Lenguaje
    Then el ususario debería obtener el ISO Code del Lenguaje "es"

  @searchErrorLenguaje
  Scenario: búsqueda  errónea de el ISO Code del Lenguaje
    Given que el usuario quiera buscar el nombre del lenguaje que no concuerda "Espanol"
    When el usuario hace la petición de busqueda de el ISO Code del Lenguaje
    Then el ususario debería obtener como resultado de la busqueda "Language name not found!"