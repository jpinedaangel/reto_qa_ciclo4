Feature: Busqueda de nombres continentes
  Como usuario del buscador
  necesito validar que la funcionalidad de busqueda de nombres de continentes trabaje adecuadamente
  para poder tener seguridad en las operaciones.

  Scenario: Buscar el continente africano
    Given que el usuario del servicio desea buscar el continente africano
    When el usuario realiza la busqueda
    Then el usuario deberia ver el resultado "Africa"

  Scenario: Buscar el continente europeo
   Given que el usuario del buscador desea buscar el continente europeo
    When el usuario del buscador ejecuta la busqueda
    Then el usuario deberia obtener la respuesta "Europe"