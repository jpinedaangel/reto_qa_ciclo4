Feature: Busqueda de informacion por medio de codigo ISO
  como usuario del buscador de informacion
  necesito realizar la busqueda por medio del codigo ISO
  para obtener la informacion correspondiente del mismo

  Scenario: Buscador capital
    Given que el usuario del buscador ha definido como codigo ISO del pais "AF"
    When el usuario del buscador ejecuta el buscador
    Then el ususario deberia obtener el resultado de la capital "Kabul"

  Scenario: Buscador de codigo de telefono
    Given que el usuario ha enviado el codigo ISO "AM"
    When el usuario ejecuta la busqueda
    Then el ususario deberia obtener el resultado de codigo telefonico "374"