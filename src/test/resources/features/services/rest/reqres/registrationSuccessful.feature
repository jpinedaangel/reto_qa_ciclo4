Feature: Registro de usuario
  AS
    usuario del sistema
  I WANT TO
    registrarse en el sistema
  SO THAT
    puedo usar los servicios del sistema.

  Scenario: Registro exitoso
    Given un usuario permitido se encuentra en la página de registro
    When el usuario envía una peticion de regitro con el email "eve.holt@reqres.in" y el password "pistol"
    Then el usuario ve un código de respuesta exitoso con su id y su token

  Scenario: Registro no permitido
    Given un usuario no permitido del website intenta registrarse
    When el usuario inteta una petición de registro con el email "snow.flake@reqres.in" y el password "snowflake"
    Then el usuario ve un código bad request y un mensaje de error