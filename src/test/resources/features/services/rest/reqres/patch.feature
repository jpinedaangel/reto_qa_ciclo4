Feature: Actualizar un registro
  como un usuario de la página quiero actualizar un registro
  asignando un nombre de usuario y un  trabajo

  Scenario: actualización Reqres exitosa
    Given el usuario está en la página Reqres ingresa el nombre "morpheus" y el campo trabajo "zion resident"
    When cuando el usuario hace una petición de actualizacion de registro
    Then el usuario deberá ver un codigo de respuesta 200 y los datos creados

  Scenario: actualización Reqres fallida
    Given el usuario está en la página de actualizacion  de Reqres
    When cuando el usuario hace una petición de actualizacion vacia
    Then el usuario deberá ver unicamente un codigo 200