Feature: Consulta de usuarios
  Yo como usuario administrador
  quiero poder consultar los usuarios registrados en el aplicativo
  para gestionar su información

  Background: Acceso al aplicativo
    Given el usuario tiene acceso al aplicativo
    When el usuario administrador consulta la lista de usuarios en la página 2
    Then obtendrá un código de respuesta exitoso

  @UserList
  Scenario: Consulta cantidad de usuarios por página
    Then obtiene en el campo "per_page" un valor de 6 usuarios
    And puede ver un total de 6 usuarios por página

  @UserList
  Scenario Outline: Consulta de un usuario de la lista
    And consulta el usuario con id 8 obtiene en email "<email>" en firstName "<firstName>" y en lastName "<lastName>"

    Examples:
      |            email                |  firstName   |  lastName  |
      |   lindsay.ferguson@reqres.in    |   Lindsay    |   Ferguson |
