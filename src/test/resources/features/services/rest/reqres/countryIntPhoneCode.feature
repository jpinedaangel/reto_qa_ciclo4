Feature: Busqueda del indicativo de telefono de cada pais
  Como usuario del buscador
  necesito buscar indicativo de telefono del pais que requiero
  para poder tener seguridad que estoy llamando donde es.

  Scenario: Buscar indicativo de telefono segun su pais
    Given que el usuario desea saber el indicativo de colombia por lo que escribe el codigo: "COP"
    When el usuario realiza la busqueda del indicativo
    Then el usuario deberia obtener el resultado del indicativo "57"

  Scenario: Indicativo de telefono incorrecto
    Given que el usuario desea conocer el indicativo de colombia por lo que escribe el codigo: "cop"
    When el usuario del buscador corre la busqueda del indicativo
    Then el usuario deberia obtener el resultado de error del indicativo "Country not found in the database"