package co.com.sofka;

public class ReqresSetup extends GeneralSetup{
    protected static final String URL_BASE_REQRES = "https://reqres.in/api";
    protected static final String RESOURCE_USERS = "/users/%s";
    protected static final String RESOURCE_REGISTER = "/register";
    protected static final String RESOURCE_LIST = "/unknown";
    protected static final String RESOURCE_SINGLE_RESOURCE= "/unknown/%s";
    protected static final String RESOURCE_LIST_USERS= "/users?page={page}";


    protected void setupReqres () {
        actorCan(URL_BASE_REQRES);
    }


}
