package co.com.sofka;

public class WebSamplesSetup extends GeneralSetup {
    protected static final String URL_BASE = "http://webservices.oorsprong.org";
    protected static final String RESOURCE = "/websamples.countryinfo/CountryInfoService.wso";

    protected void setupWebSamples () {
        actorCan(URL_BASE);
    }

}
