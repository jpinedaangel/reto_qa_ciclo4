package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.CountryNameModel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.countryname.Patch.COUNTRYNAME;
import static co.com.sofka.util.services.soap.countryname.Response.COUNTRYNAME_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CountryNameStepDefinition extends WebSamplesSetup {

    private static final Logger LOGGER = Logger.getLogger(CountryNameStepDefinition.class);
    CountryNameModel countryName = new CountryNameModel();

    @Given("que el usuario quiere buscar el país con código ISO {string}")
    public void queElUsuarioQuiereBuscarElPaisConCodigoISO(String countryCode) {

        try {
            super.setupWebSamples();
            countryName = new CountryNameModel();
            countryName.setIsoCode(countryCode);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }


    @When("el usuario hace la petición y confirma la acción")
    public void elUsuarioHaceLaPeticionYConfirmaLaAccion() {
        try {
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }


    @Then("el usuario debería ver el nombre del país {string}")
    public void elUsuarioDeberiaVerElNombreDelPais(String country) {
        try {
            countryName.setCountryName(country);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la peticion es :  ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Given("que el usuario desea buscar el país con código ISO {string}")
    public void queElUsuarioDeseaBuscarElPaisConCodigoISO(String wrongCode) {
        try {
            super.setupWebSamples();
            countryName = new CountryNameModel();
            countryName.setIsoCode(wrongCode);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @When("el usuario introduce los datos de búsqueda y envía la nueva solicitud")
    public void elUsuarioIntroduceLosDatosDeBusquedaYEnviaLaNuevaSolicitud() {
        try {
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Then("el usuario debería ver el mensaje {string}")
    public void elUsuarioDeberiaVerElMensaje(String countryMessage) {
        try {
            countryName.setCountryName(countryMessage);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la peticion es :  ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }


    private String bodyRequest () {
        return String.format(readFile(COUNTRYNAME.getValue()), countryNameModel().getIsoCode());
    }

    private String bodyResponse () {
        return String.format(COUNTRYNAME_RESPONSE.getValue(), countryNameModel().getCountryName());
    }

    private CountryNameModel countryNameModel () {
        return countryName;
    }

}




