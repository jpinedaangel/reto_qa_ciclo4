package co.com.sofka.stepdefinitions.services.soap;
import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.WebSamplesModels;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.intphonecountry.PatchIntPhone.BUSCAR_INT_PHONE;
import static co.com.sofka.util.services.soap.intphonecountry.ResponseIntPhone.SEARCH_RESPONSE_INT_PHONE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CountryIntPhoneCodeStepDefinition extends WebSamplesSetup {

    public static Logger log = Logger.getLogger(CountryIntPhoneCodeStepDefinition.class);
    private WebSamplesModels webSamplesModels;

    @Given("que el usuario desea saber el indicativo de colombia por lo que escribe el codigo: {string}")
    public void queElUsuarioDeseaSaberElIndicativoDeColombiaPorLoQueEscribeElCodigo(String codeCountry) {
        try {
            log.info("Se inicializa las configuraciones");
            super.setupWebSamples();

            webSamplesModels = new WebSamplesModels();
            webSamplesModels.setCode(codeCountry);
        }catch (Exception e){
            log.info("Error en la configuración general");
        }

    }
    @When("el usuario realiza la busqueda del indicativo")
    public void elUsuarioRealizaLaBusquedaDelIndicativo() {
        try{
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
            log.info("Se carga el indicativo del pais");
        }catch (Exception e){
            log.info("Fallo en la carga del indicativo");
        }
    }
    @Then("el usuario deberia obtener el resultado del indicativo {string}")
    public void elUsuarioDeberiaObtenerElResultadoDelIndicativo(String intPhone) {

        try{
            webSamplesModels.setAnswer(intPhone);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
            log.info("Se comparan las respuestas");
        } catch (Exception e){
            log.info("Hubo fallos en el dato de salida");}

    }

    @Given("que el usuario desea conocer el indicativo de colombia por lo que escribe el codigo: {string}")
    public void queElUsuarioDeseaConocerElIndicativoDeColombiaPorLoQueEscribeElCodigo(String codeCountry) {
        try {
            log.info("Se inicializa las configuraciones");
            super.setupWebSamples();
            webSamplesModels = new WebSamplesModels();
            webSamplesModels.setCode(codeCountry);
        }catch (Exception e){
            log.info("Error en la configuración general");
        }

    }

    @When("el usuario del buscador corre la busqueda del indicativo")
    public void elUsuarioDelBuscadorCorreLaBusquedaDelIndicativo() {
        try{
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
            log.info("Se carga el indicativo del pais");
        }catch (Exception e){
            log.info("Fallo en la carga del indicativo");
        }

    }
    @Then("el usuario deberia obtener el resultado de error del indicativo {string}")
    public void elUsuarioDeberiaObtenerElResultadoDeErrorDelIndicativo(String intPhoneError) {
        try{
            webSamplesModels.setAnswer(intPhoneError);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
            log.info("Se comparan las respuestas");
        } catch (Exception e){
            log.info("Hubo fallos en el dato de salida");}


    }


    private WebSamplesModels webSamplesModels(){
        return webSamplesModels;
    }

    private String bodyRequest(){
        return String.format(readFile(BUSCAR_INT_PHONE.getValue()), webSamplesModels().getCode());
    }
    private String bodyResponse(){
        return String.format(SEARCH_RESPONSE_INT_PHONE.getValue(), webSamplesModels().getAnswer());
    }

}
