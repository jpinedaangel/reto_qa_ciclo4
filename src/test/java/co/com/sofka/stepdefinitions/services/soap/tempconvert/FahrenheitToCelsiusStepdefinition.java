package co.com.sofka.stepdefinitions.services.soap.tempconvert;

import co.com.sofka.TempConvertSetup;
import co.com.sofka.model.services.soap.TemperatureModel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.Path.TEMP_CONVERT_PATH;
import static co.com.sofka.util.services.soap.Responses.TEMP_CONVERT_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.containsString;

public class FahrenheitToCelsiusStepdefinition extends TempConvertSetup {

    private TemperatureModel temperatureModel;
    private static final Logger LOGGER = Logger.getLogger(FahrenheitToCelsiusStepdefinition.class);

    @Given("que el usuario tiene acceso al aplicativo conversor de temperatura")
    public void queElUsuarioTieneAccesoAlAplicativoConversorDeTemperatura() {
        super.setupTempConvert();
        LOGGER.info("Se pudo acceder al aplicativo");
    }

    @When("el usuario realiza una conversión de temperatura de {double} grados Fahrenheit a grados Celsius")
    public void elUsuarioRealizaUnaConversionDeTemperaturaDeGradosFahrenheitAGradosCelsius(Double fahrenheitDegrees) {
    temperatureModel = new TemperatureModel();
    temperatureModel.setFahrenheitTemperature(String.valueOf(fahrenheitDegrees));
    actor.attemptsTo(
            doPost()
                    .withTheResource(RESOURCE)
                    .andTheHeaders(super.headers(HEADER_FTC))
                    .andTheBodyRequest(bodyRequest())
    );
        LOGGER.info("Se pudo realizar la petición");
    }

    @Then("obtiene {int} grados Celsius como resultado de la conversión")
    public void obtieneGradosCelsiusComoResultadoDeLaConversion(Integer celsiusDegrees) {
        temperatureModel.setCelsiusTemperature(String.valueOf(celsiusDegrees));
        actor.should(
                seeThat("El resultado debe ser:",
                        resturnSoapServiceResponse(),
                        containsString(bodyResponse()))
        );
        LOGGER.info("Se obtuvo el resultado esperado");
    }

    @When("el usuario intenta realizar una conversión de temperatura a grados Celsius con un dato de tipo string {string} no válido")
    public void elUsuarioIntentaRealizarUnaConversionDeTemperaturaAGradosCelsiusConUnDatoDeTipoStringNoValido(String fahrenheitDegrees) {
        temperatureModel = new TemperatureModel();
        temperatureModel.setFahrenheitTemperature(fahrenheitDegrees);
        actor.attemptsTo(
                doPost()
                        .withTheResource(RESOURCE)
                        .andTheHeaders(super.headers(HEADER_FTC))
                        .andTheBodyRequest(bodyRequest())
        );
        LOGGER.info("Se pudo realizar la petición");
    }

    @Then("obtiene como respuesta un mensaje de {string}")
    public void obtieneComoRespuestaUnMensajeDe(String message) {
        temperatureModel.setCelsiusTemperature(message);
        actor.should(
                seeThat("Obtiene un mensaje de error:",
                        resturnSoapServiceResponse(),
                        containsString(bodyResponse()))
        );
        LOGGER.info("Se obtuvo el resultado esperado");
    }
    private TemperatureModel temperatureModel(){
        return temperatureModel;
    }

    private String bodyRequest(){
        return String.format(readFile(TEMP_CONVERT_PATH.getValue()), temperatureModel.getFahrenheitTemperature());
    }

    private String bodyResponse(){
        return String.format(TEMP_CONVERT_RESPONSE.getValue(), temperatureModel.getCelsiusTemperature());
    }
}
