package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.FullCountryInfoModel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.fullcountryinfo.Patch.FULLCOUNTRYINFO;

import static co.com.sofka.util.services.soap.fullcountryinfo.Response.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class FullCountryInfo extends WebSamplesSetup {
    private static final Logger LOGGER = Logger.getLogger(FullCountryInfo.class);
    private FullCountryInfoModel fullCountryInfoModel;

    @Given("que el usuario del buscador ha definido como codigo ISO del pais {string}")
    public void queElUsuarioDelBuscadorHaDefinidoComoCodigoISODelPais(String codeAa) {
        try {
            super.setupWebSamples();
            fullCountryInfoModel = new FullCountryInfoModel();
            fullCountryInfoModel.setCodeA(codeAa);
        } catch (Exception exception) {
            LOGGER.warn(exception.getMessage());
            Assertions.fail("");
        }
    }

    @When("el usuario del buscador ejecuta el buscador")
    public void elUsuarioDelBuscadorEjecutaElBuscador() {
        try{
        actor.attemptsTo(
                doPost()
                        .withTheResource(RESOURCE)
                        .andTheHeaders(super.headers())
                        .andTheBodyRequest(cuerpo(true))
        );
        } catch (Exception exception) {
            LOGGER.warn(exception.getMessage());
            Assertions.fail("");
        }
    }

    @Then("el ususario deberia obtener el resultado de la capital {string}")
    public void elUsusarioDeberiaObtenerElResultadoDeLaCapital(String capital) {
        fullCountryInfoModel.setCapital(capital);

        try {
        actor.should(
                seeThatResponse("El código de rspuesta HTTP debe ser:  ",
                        response -> response.statusCode(HttpStatus.SC_OK)),
                seeThat("La capital es:  ",
                        resturnSoapServiceResponse(),
                        containsString(cityResponse()))
        );
            LOGGER.info("Comparacion exitosa de la capital");
        } catch (Exception exception) {
            LOGGER.warn(exception.getMessage());
            Assertions.fail("");
        }
    }

    @Given("que el usuario ha enviado el codigo ISO {string}")
    public void queElUsuarioDelBusacadorHaDefinidoElCodigoISO(String B) {
        try {
        super.setupWebSamples();
        fullCountryInfoModel = new FullCountryInfoModel();
        fullCountryInfoModel.setCodeB(B);
        } catch (Exception exception) {
            LOGGER.warn(exception.getMessage());
            Assertions.fail("");
        }
    }

    @When("el usuario ejecuta la busqueda")
    public void elUsuarioDelBuscadorEjecutaLaBusqueda() {
        try {
        actor.attemptsTo(
                doPost()
                        .withTheResource(RESOURCE)
                        .andTheHeaders(super.headers())
                        .andTheBodyRequest(cuerpo(false))
        );
        } catch (Exception exception) {
            LOGGER.warn(exception.getMessage());
            Assertions.fail("");
        }
    }

    @Then("el ususario deberia obtener el resultado de codigo telefonico {string}")
    public void elUsusarioDeberiaObtenerElResultadoDeCodigoTelefonico(String codeTele) {
        fullCountryInfoModel.setCodeTele(codeTele);
        try {
        actor.should(
                seeThatResponse("El código de rspuesta HTTP debe ser:  ",
                        response -> response.statusCode(HttpStatus.SC_OK)),
                seeThat("El codigo de telefono es:  ",
                        resturnSoapServiceResponse(),
                        containsString(numResponse()))
        );
            LOGGER.info("Comparacion exitosa de codigo telefonico");
        } catch (Exception exception) {
            LOGGER.warn(exception.getMessage());
            Assertions.fail("");
        }
    }

    private FullCountryInfoModel fullCountryInfoModel() {
        return fullCountryInfoModel;
    }

    private String cuerpo(boolean code) {
        if (code) {
            return String.format(readFile(FULLCOUNTRYINFO.getValue()), fullCountryInfoModel().getCodeA());
        }
        return String.format(readFile(FULLCOUNTRYINFO.getValue()), fullCountryInfoModel().getCodeB());
    }


    private String cityResponse() {
        return String.format(FULLCOUNTRYINFO_C_RESPONSE.getValue(), fullCountryInfoModel().getCapital());
    }

    private String numResponse() {
        return String.format(FULLCOUNTRYINFO_NUM_RESPONSE.getValue(), fullCountryInfoModel().getCodeTele());
    }
}
