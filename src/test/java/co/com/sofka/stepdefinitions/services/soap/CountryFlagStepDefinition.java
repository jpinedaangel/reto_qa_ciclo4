package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.CountryFlagModel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.countryflag.Patch.COUNTRYFLAG;
import static co.com.sofka.util.services.soap.countryflag.Response.COUNTRYFLAG_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CountryFlagStepDefinition extends WebSamplesSetup {

    private static final Logger LOGGER = Logger.getLogger(CountryNameStepDefinition.class);
    CountryFlagModel countryFlagModel = new CountryFlagModel();

    @Given("que el usuario quiere buscar la bandera del país con código ISO {string}")
    public void queElUsuarioQuiereBuscarLaBanderaDelPaisConCodigoISO(String countryCode){

        try {
            super.setupWebSamples();
            countryFlagModel = new CountryFlagModel();
            countryFlagModel.setIsoCode(countryCode);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @When("el usuario realiza la petición y confirma la acción")
    public void elUsuarioRealizaLaPeticionYConfirmaLaAccion() {
        try {
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Then("el usuario debería poder visualizar la url de la bandera del pais {string}")
    public void elUsuarioDeberiaPoderVisualizarLaUrlDeLaBanderaDelPais(String flag) {
        try {
            countryFlagModel.setUrlFlag(flag);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la peticion es :  ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Given("que el usuario quiere hacer la busqueda del país con código ISO {string}")
    public void queElUsuarioQuiereHacerLaBusquedaDelPaisConCodigoISO(String wrongCode) {
        try {
            super.setupWebSamples();
            countryFlagModel = new CountryFlagModel();
            countryFlagModel.setIsoCode(wrongCode);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @When("el usuario inserta los datos de búsqueda y envía la solicitud")
    public void elUsuarioInsertaLosDatosDeBusquedaYEnviaLaSolicitud() {
        try {
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    @Then("el usuario vera el mensaje {string}")
    public void elUsuarioVeraElMensaje(String urlFlag) {
        try {
            countryFlagModel.setUrlFlag(urlFlag);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la peticion es :  ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    private String bodyRequest () {
        return String.format(readFile(COUNTRYFLAG.getValue()), countryFlagModel().getIsoCode());
    }

    private String bodyResponse () {
        return String.format(COUNTRYFLAG_RESPONSE.getValue(), countryFlagModel().getUrlFlag());
    }

    private CountryFlagModel countryFlagModel () {
        return countryFlagModel;
    }
}
