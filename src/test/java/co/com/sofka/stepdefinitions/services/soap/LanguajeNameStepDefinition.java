package co.com.sofka.stepdefinitions.services.soap;
import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.WebSamplesModels;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.languajename.PatchLanguajeName.BUSCAR;
import static co.com.sofka.util.services.soap.languajename.ResponseLanguajeName.SEARCH_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class LanguajeNameStepDefinition extends WebSamplesSetup {

    private static final Logger LOGGER = Logger.getLogger(LanguajeNameStepDefinition.class);
    private WebSamplesModels webSamplesModels;

    @Given("que el usuario del buscador ha definido el codigo ISO {string}")
    public void queElUsuarioDelBuscadorHaDefinidoElCodigoISO(String code) {
        try {
            LOGGER.info("Se inicializa las configuraciones");
            super.setupWebSamples();

            webSamplesModels = new WebSamplesModels();
            webSamplesModels.setCode(code);
        }catch (Exception e){
            LOGGER.info("Error en la configuración general");
        }

    }
    @When("el usuario del buscador realiza la busqueda")
    public void elUsuarioDelBuscadorRealizaLaBusqueda() {
        try{
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
            LOGGER.info("Se carga el codigo del idioma");
        }catch (Exception e){
            LOGGER.info("Fallo en la carga del codigo");
        }
    }
    @Then("el usuario deberia obtener el resultado {string}")
    public void elUsuarioDeberiaObtenerElResultado(String outcome) {

        try{
            webSamplesModels.setAnswer(outcome);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
            LOGGER.info("Se comparan las respuestas");
        } catch (Exception e){
            LOGGER.info("Hubo fallos en el dato de salida");
            LOGGER.warn(e.getMessage());}

    }

    @Given("que el usuario del buscador ha definido el codigo ISO incorrecto {string}")
    public void queElUsuarioDelBuscadorHaDefinidoElCodigoISOIncorrecto(String code) {
        try {
            LOGGER.info("Se inicializa las configuraciones");
            super.setupWebSamples();
            webSamplesModels = new WebSamplesModels();
            webSamplesModels.setCode(code);
        }catch (Exception e){
            LOGGER.info("Error en la configuración general");
        }

    }

    @When("el usuario del buscador corre la busqueda")
    public void elUsuarioDelBuscadorCorreLaBusqueda() {
        try{
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
            LOGGER.info("Se carga el codigo del idioma");
        }catch (Exception e){
            LOGGER.info("Fallo en la carga del codigo");
        }

    }
    @Then("el usuario deberia obtener el resultado incorrecto {string}")
    public void elUsuarioDeberiaObtenerElResultadoIncorrecto(String IncorrectOut) {
        try{
            webSamplesModels.setAnswer(IncorrectOut);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
            LOGGER.info("Se comparan las respuestas");
        } catch (Exception e){
            LOGGER.info("Hubo fallos en el dato de salida");
            LOGGER.warn(e.getMessage());}


    }


    private WebSamplesModels webSamplesModels(){
        return webSamplesModels;
    }

    private String bodyRequest(){
        return String.format(readFile(BUSCAR.getValue()), webSamplesModels().getCode());
    }
    private String bodyResponse(){
        return String.format(SEARCH_RESPONSE.getValue(), webSamplesModels().getAnswer());
    }

}
