package co.com.sofka.stepdefinitions.services.rest.reqres;

import co.com.sofka.ReqresSetup;
import co.com.sofka.model.services.rest.UserListData;
import co.com.sofka.question.services.rest.GetListUserData;
import co.com.sofka.question.services.rest.ResponseCode;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import java.util.Objects;

import static co.com.sofka.task.services.rest.DoGetByPage.doGet;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public class ConsultListUsersStepdefinition extends ReqresSetup {

    protected static final Logger LOGGER = Logger.getLogger(ConsultListUsersStepdefinition.class);

    @Given("el usuario tiene acceso al aplicativo")
    public void elUsuarioTieneAccesoAlAplicativo() {
        super.setupReqres();
        LOGGER.info("Se pudo acceder al aplicativo");
    }

    @When("el usuario administrador consulta la lista de usuarios en la página {int}")
    public void elUsuarioAdministradorConsultaLaListaDeUsuariosEnLaPagina(Integer page) {
        actor.attemptsTo(
                doGet()
                        .withTheResource(RESOURCE_LIST_USERS)
                        .withThePathParam(page)
        );
        LOGGER.info("Se pudo realizar la petición");
    }

    @Then("obtendrá un código de respuesta exitoso")
    public void obtendraUnCodigoDeRespuestaExitoso() {
        actor.should(
                seeThat("El código de respuesta HTTP debe ser: ",
                        ResponseCode.was(),equalTo(HttpStatus.SC_OK))
        );
        LOGGER.info("Obtuvo el código de respuesta esperado");
    }

    @Then("obtiene en el campo {string} un valor de {int} usuarios")
    public void obtieneEnElCampoUnValorDeUsuarios(String field, Integer usersAmount) {
        actor.should(
                seeThatResponse("El valor del campo es:" + usersAmount,
                        response -> response.body(field,equalTo(usersAmount)))
        );
        LOGGER.info("Obtuvo la respuesta esperada");
    }

    @And("puede ver un total de {int} usuarios por página")
    public void puedeVerUnTotalDeUsuariosPorPagina(Integer usersAmount) {

        actor.should(
                seeThat("El total de usuarios por página",
                        act -> getSizeListUser(),equalTo(usersAmount))
        );
        LOGGER.info("Obtuvo la respuesta esperada");
    }

    @And("consulta el usuario con id {int} obtiene en email {string} en firstName {string} y en lastName {string}")
    public void consultaElUsuarioConIdObtieneEnEmailEnFirstNameYEnLastName(Integer id, String email, String firstName, String lastName) {

        UserListData user = GetListUserData.inTheResponse().answeredBy(actor).stream()
                .filter(x -> Objects.equals(x.getId(), id)).findFirst().orElse(null);

        actor.should(
                seeThat("El usuario no es nulo", act -> user,notNullValue())
        );
        actor.should(
                seeThat("El email", act ->user.getEmail(), equalTo(email)),
                seeThat("El primer nombre", act -> user.getFirstName(), equalTo(firstName)),
                seeThat("El apellido", act -> user.getLastName(), equalTo(lastName))
        );
        LOGGER.info("Obtuvo los resultados esperados");
    }

    private int getSizeListUser(){
        return GetListUserData.inTheResponse().answeredBy(actor).size();
    }
}
