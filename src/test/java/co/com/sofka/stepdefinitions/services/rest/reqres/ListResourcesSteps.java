package co.com.sofka.stepdefinitions.services.rest.reqres;

import co.com.sofka.ReqresSetup;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import java.io.File;

import static co.com.sofka.task.services.rest.DoGetFull.doGet;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;
import static co.com.sofka.util.services.rest.PathFiles.*;
public class ListResourcesSteps extends ReqresSetup {
    private static final Logger logger = LogManager.getLogger(ListResourcesSteps.class);

    @When("el usuario ejecute la petición para obtener la lista completa de recursos")
    public void elUsuarioEjecuteLaPeticionParaObtenerLaListaCompletaDeRecursos() {
        try {
            super.actorCan(URL_BASE_REQRES);
            actor.attemptsTo(
                    doGet()
                            .withTheResource(RESOURCE_LIST)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest("")
            );
        } catch (Exception e) {
            logger.warn("error petición\n" + e.getMessage());
            Assertions.fail(e.getMessage());

        }
    }

    @Then("el usuario deberá obtener la lista completa de recursos")
    public void elUsuarioDeberaObtenerLaListaCompletaDeRecursos() {
        try {
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            resp -> {
                                resp.statusCode(HttpStatus.SC_OK);
                                logger.info("código de respuesta: "+resp.extract().statusCode());
                            }
                    ));
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e.getMessage());
            Assertions.fail(e.getMessage());
        }
    }

    @When("el usuario ejecute la petición con {string} como criterio de busqueda")
    public void elUsuarioEjecuteLaPeticionConComoCriterioDeBusqueda(String recursoBuscado) {
        try {
            super.actorCan(URL_BASE_REQRES);
            actor.attemptsTo(
                    doGet()
                            .withTheResource(recursoBuscado)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest("")
            );
        } catch (Exception e) {
            logger.warn("error petición\n" + e.getMessage());
            Assertions.fail(e.getMessage());
        }
    }
    @Then("el usuario deberá obtener un errror")
    public void elUsuarioDeberaObtenerUnErrror() {
        try {
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            resp -> {
                                resp.statusCode(HttpStatus.SC_NOT_FOUND);
                                logger.info("código de respuesta: "+resp.extract().statusCode());
                            }
                    ));
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e.getMessage());
            Assertions.fail(e.getMessage());
        }
    }
}
