package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.WebSamplesModels;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.listofcontinentsbyname.PatchListOfContinentsByName.LIST_OF_CONTINENTS_BY_NAME;
import static co.com.sofka.util.services.soap.listofcontinentsbyname.ResponseListOfContinentsByName.RESPONSE_LIST_OF_CONTINENTS_BY_NAME;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class ListOfContinentsByNameStepDefinition  extends WebSamplesSetup {
    private static final Logger LOGGER = Logger.getLogger(LanguajeNameStepDefinition.class);
    private WebSamplesModels webSamplesModels;

    @Given("que el usuario del servicio desea buscar el continente africano")
    public void queElUsuarioDelServicioDeseaBuscarElContinenteAfricano() {
        try {
            LOGGER.info("Se inicializa las configuraciones");
            super.setupWebSamples();
            webSamplesModels = new WebSamplesModels();
        }catch (Exception e){
            LOGGER.info("Error en la configuración general");
        }

    }

    @When("el usuario realiza la busqueda")
    public void elUsuarioRealizaLaBusqueda() {
        try{
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
            LOGGER.info("Se carga el cuerpo para solicitar la lista de continentes");
        }catch (Exception e){
            LOGGER.info("Fallo en la carga del cuerpo");
        }

    }

    @Then("el usuario deberia ver el resultado {string}")
    public void elUsuarioDeberiaVerElResultado(String continetName) {
        try{
            webSamplesModels.setAnswer(continetName);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
            LOGGER.info("Se comparan las respuestas de contientes");
        } catch (Exception e){
            LOGGER.info("Hubo fallos en el dato de salida de continentes");
            LOGGER.warn(e.getMessage());}

    }
 //Scenario 2
    @Given("que el usuario del buscador desea buscar el continente europeo")
    public void queElUsuarioDelBuscadorDeseaBuscarElContinenteEuropeo() {
        try {
            LOGGER.info("Se inicializa las configuraciones");
            super.setupWebSamples();
            webSamplesModels = new WebSamplesModels();
        }catch (Exception e){
            LOGGER.info("Error en la configuración general");
        }

    }

    @When("el usuario del buscador ejecuta la busqueda")
    public void elUsuarioDelBuscadorEjecutaLaBusqueda() {
        try{
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
            LOGGER.info("Se carga el cuerpo para solicitar lista de continentes");
        }catch (Exception e){
            LOGGER.info("Fallo en la carga del cuerpo");
        }

    }

    @Then("el usuario deberia obtener la respuesta {string}")
    public void elUsuarioDeberiaObtenerLaRespuesta(String continetName) {
        try{
            webSamplesModels.setAnswer(continetName);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
            LOGGER.info("Se comparan las respuestas de continentes");
        } catch (Exception e){
            LOGGER.info("Hubo fallos en el dato de salida");
        LOGGER.warn(e.getMessage());
        }

    }

    private WebSamplesModels webSamplesModels(){
        return webSamplesModels;
    }

    private String bodyRequest(){
        return String.format(readFile(LIST_OF_CONTINENTS_BY_NAME.getValue()), webSamplesModels().getCode());
    }
    private String bodyResponse(){
        return String.format(RESPONSE_LIST_OF_CONTINENTS_BY_NAME.getValue(), webSamplesModels().getAnswer());
    }


}
