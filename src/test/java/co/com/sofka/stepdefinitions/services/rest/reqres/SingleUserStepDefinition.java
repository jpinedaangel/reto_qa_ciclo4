package co.com.sofka.stepdefinitions.services.rest.reqres;

import co.com.sofka.ReqresSetup;
import co.com.sofka.model.services.rest.UserListData;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import static co.com.sofka.question.services.rest.SingleUserResponse.singleUserResponse;
import static co.com.sofka.task.services.rest.DoGet.doGet;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;

public class SingleUserStepDefinition extends ReqresSetup {

    private static final Logger LOGGER = Logger.getLogger(SingleUserStepDefinition.class);
    private String userId;

    //Background
    @Given("el administrador está en la pagina web de reqres y quiere enumerar la informacion de un usuario")
    public void el_administrador_esta_en_la_pagina_web_de_reqres_y_quiere_enumerar_la_informacion_de_un_usuario() {

        try {
            super.setupReqres();
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    //Escenario Single User Unico Usuario
    @When("el administrador ingresa a la base de datos y escoge el usuario {string}")
    public void el_administrador_ingresa_a_la_base_de_datos_y_escoge_el_usuario(String id) {
        try {
            userId = id;
            actor.attemptsTo(
                    doGet()
                            .withTheResource(String.format(RESOURCE_USERS, id))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    @Then("el sistema imprime en pantalla la respuesta exitosa y los datos del usuario")
    public void el_sistema_imprime_en_pantalla_la_respuesta_exitosa_y_los_datos_del_usuario() {

        try {
            UserListData userResponse = singleUserResponse().answeredBy(actor).getData();
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: " + HttpStatus.SC_OK,
                            response -> response.statusCode(HttpStatus.SC_OK)
                    ),
                    seeThat("Recupera la información de un usuario: ",
                            act -> userResponse, notNullValue()
                    )
            );
            actor.should(
                    seeThat("El id de del usuario recuperado es: ",
                            act -> userResponse.getId(), equalTo(Integer.valueOf(userId)))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());

        }
    }

    //Escenario Single User Usuario Inexistente
    @When("el administrador ingresa al sistema y escoge el usuario {string}")
    public void el_administrador_ingresa_al_sistema_y_escoge_el_usuario(String id) {

        try {
            actor.attemptsTo(
                    doGet()
                            .withTheResource(String.format(RESOURCE_USERS, id))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());

        }

    }

    @Then("el sistema debe mostrar en pantalla usuario no encontrado")
    public void el_sistema_debe_mostrar_en_pantalla_usuario_no_encontrado() {

        try {
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: " + HttpStatus.SC_NOT_FOUND,
                            response -> response.statusCode(HttpStatus.SC_NOT_FOUND)
                    )
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }
}