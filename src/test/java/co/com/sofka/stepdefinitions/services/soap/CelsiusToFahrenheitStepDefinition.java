package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.TempConvertSetup;
import co.com.sofka.model.services.soap.TempConvertCelsiusToFahrenheit;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.celsiustofahrenheit.Patch.CONVERT;
import static co.com.sofka.util.services.soap.celsiustofahrenheit.Response.CONVERT_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CelsiusToFahrenheitStepDefinition extends TempConvertSetup {

    private static final Logger LOGGER = Logger.getLogger(CelsiusToFahrenheitStepDefinition.class);

    //Escenario Celsius To Fahrenheit Punto De Congelacion
    private TempConvertCelsiusToFahrenheit tempConvertC;

    @Given("que el usuario necesita saber el punto de congelacion del agua en grados Fahrenheit")
    public void queElUsuarioNecesitaSaberElPuntoDeCongelacionDelAguaEnGradosFahrenheit() {

        try {
            super.setupTempConvert();

        }catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }

    @When("el usuario digite {int} grados celsius y ejecute el calculo de conversión de temperatura")
    public void elUsuarioDigiteGradosCelsiusYEjecuteElCalculoDeConversionDeTemperatura(Integer conversionA) {

        try {
            tempConvertC = new TempConvertCelsiusToFahrenheit();
            tempConvertC.setA(conversionA);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers(HEADER_CTF))
                            .andTheBodyRequest(bodyRequestC())

            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }

    @Then("el usuario debera obtener como resultado {int} grados")
    public void elUsuarioDeberaObtenerComoResultadoGrados(Integer resultadoC) {

       tempConvertC.setOutcome(resultadoC);

        try{
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la conversion debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponseC()))
            );

        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail("error en el resulatdo de la conversion" + e);
        }
    }

    private TempConvertCelsiusToFahrenheit tempConvertC(){
        return tempConvertC;
    }


    //Escenario Celsius To Fahrenheit Punto De Ebullicion
    @Given("que el usuario quiere conocer punto de ebullición en grados Fahrenheit")
    public void queElUsuarioQuiereConocerPuntoDeEbullicionEnGradosFahrenheit() {

       try {
            super.setupTempConvert();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
           Assertions.fail(e.getMessage());
        }
    }

    @When("el usuario ingrese {int} grados celsius")
    public void elUsuarioIngreseGradosCelsius(Integer conversionB) {

       try {
            tempConvertC = new TempConvertCelsiusToFahrenheit();
            tempConvertC.setA(conversionB);

            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers(HEADER_CTF))
                            .andTheBodyRequest(bodyRequestC())
            );

        }catch (Exception e) {
           LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage(),e);
        }
    }

    @Then("obtendra como resultado {int} grados Fahrenheit")
    public void obtendraComoResultadoGradosFahrenheit(Integer resultadoE) {


        try{
            tempConvertC.setOutcome(resultadoE);
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la conversion debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponseC()))

            );

        }catch (Exception exception) {
            LOGGER.warn(exception.getMessage(), exception);
            Assertions.fail("error en el resulatdo de la conversion" + exception);

        }

    }

    private String bodyRequestC(){
        return String.format(readFile(CONVERT.getValue()), tempConvertC().getA());

    }
    private String bodyResponseC(){
        return String.format(CONVERT_RESPONSE.getValue(),tempConvertC().getOutcome());

    }
}