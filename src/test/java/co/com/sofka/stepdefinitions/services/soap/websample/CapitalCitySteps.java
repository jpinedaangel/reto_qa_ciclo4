package co.com.sofka.stepdefinitions.services.soap.websample;

import co.com.sofka.GeneralSetup;

import co.com.sofka.util.services.soap.Responses;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;


import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;

import static co.com.sofka.util.services.soap.PathFiles.CAPITAL_CITY_REQUEST;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CapitalCitySteps extends GeneralSetup {
    private static final Logger logger = LogManager.getLogger(CapitalCitySteps.class);
    @When("el usuario ejecute la petición con código {string}  para buscar una ciudad capital")
    public void elUsuarioEjecuteLaPeticionConCodigoParaBuscarUnaCiudadCapital(String isoCode) {
      try {
          super.actorCan(URL_BASE_WEB_EXAMPLE);
          actor.attemptsTo(
                  doPost()
                          .withTheResource(RESOURCE_WEB_EXAMPLE)
                          .andTheHeaders(super.headers())
                          .andTheBodyRequest(BodyPetition.bodyRequest(
                                  CAPITAL_CITY_REQUEST.getValue(),isoCode))
          );
      }catch (Exception e){
          logger.warn("error petición\n"+e.getMessage());
          Assertions.fail(e.getMessage());

        }
    }

    @Then("el usuario debería obtener el nombre de ciudad {string}")
    public void elUsuarioDeberiaObtenerElNombreDeCiudad(String response) {
      try {
           actor.should(
                   seeThatResponse("El código de rspuesta HTTP debe ser: ",
                           resp -> resp.statusCode(HttpStatus.SC_OK)),
                   seeThat("La ciudad capital es: ",
                           resturnSoapServiceResponse(),
                           containsString(BodyPetition.bodyResponse(
                                   Responses.CAPITAL_CITY_RESPONSE.getValue(),response)))
           );
       }catch (AssertionError e){
          logger.warn("Error en la validación\n"+e);
           Assertions.fail(e.getMessage());

       }
    }

    @Then("el sistema no encontrará ninguna ciudad y la respuesta será {string}")
    public void elSistemaNoEncontraraNingunaCiudadYLaRespuestaSera(String response) {
       try {
            actor.should(
                    seeThatResponse("El código de rspuesta HTTP debe ser: ",
                            resp -> resp.statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)),
                    seeThat("Respuesta del servidor: ",
                            resturnSoapServiceResponse(),
                            containsString(BodyPetition.bodyResponse(
                                    Responses.ERROR_SERVER_RESPONSE.getValue(),response)))
            );
        }catch (AssertionError e) {
           logger.warn("Error en la validación\n"+e);
           Assertions.fail(e.getMessage());

        }
    }
}
