package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.WebSamplesModels;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.currencyname.Patch.CURRENCY_NAME;
import static co.com.sofka.util.services.soap.currencyname.Response.CURRENCY_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CurrencyNameStepDefinition extends WebSamplesSetup {
    private static final Logger LOGGER = Logger.getLogger(CurrencyNameStepDefinition.class);

    private WebSamplesModels webSamplesModels;

    @Given("que el usuario quiera buscar el nombre de la moneda")
    public void queElUsuarioQuieraBuscarElNombreDeLaMoneda () {
        try {
            super.setupWebSamples();

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");
        }

    }


    @When("el usuario hace la petición de busqueda de el codigo {string}")
    public void elUsuarioHaceLaPeticionDeBusquedaDeElCodigo (String isoCode) {
        try {
            webSamplesModels = new WebSamplesModels();
            webSamplesModels.setCode(isoCode);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    @Then("el ususario debería obtener como moneda {string}")
    public void elUsusarioDeberiaObtenerComoMoneda (String answer) {
        try {
            webSamplesModels.setAnswer(answer);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda es : ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }


    @Given("que el usuario quiera buscar por nombre no valido")
    public void queElUsuarioQuieraBuscarPorNombreNoValido () {
        try {
            super.setupWebSamples();

        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    @When("el usuario hace la petición de busqueda del codigo {string}")
    public void elUsuarioHaceLaPeticionDeBusquedaDelCodigo (String isoCode) {
        try {
            webSamplesModels = new WebSamplesModels();
            webSamplesModels.setCode(isoCode);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }


    }

    @Then("el ususario debería obtener como resultado {string}")
    public void elUsusarioDeberiaObtenerComoResultado (String answer) {
        try {
            webSamplesModels.setAnswer(answer);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: ",
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la busqueda es : ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    private WebSamplesModels webSamplesModels () {
        return webSamplesModels;
    }

    private String bodyRequest () {
        return String.format(readFile(CURRENCY_NAME.getValue()), webSamplesModels().getCode());
    }

    private String bodyResponse () {
        return String.format(CURRENCY_RESPONSE.getValue(), webSamplesModels().getAnswer());
    }

}
