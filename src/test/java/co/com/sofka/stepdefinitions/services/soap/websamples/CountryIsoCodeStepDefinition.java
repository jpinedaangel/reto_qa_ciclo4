package co.com.sofka.stepdefinitions.services.soap.websamples;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.CountryISOCodeModel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.countryinfo.Path.COUNTRY_ISO_CODE;
import static co.com.sofka.util.services.soap.countryinfo.Response.COUNTRY_ISO_CODE_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CountryIsoCodeStepDefinition extends WebSamplesSetup {
    private static final Logger LOGGER = Logger.getLogger(CountryIsoCodeStepDefinition.class);
    private CountryISOCodeModel countryISOCodeModel;

    @Given("un usuario desea conocer el código ISO de un país")
    public void unUsuarioDeseaConocerElCodigoISODeUnPais() {
        try{
            super.setupWebSamples();
            countryISOCodeModel = new CountryISOCodeModel();
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    // First Scenario.
    @When("el usuario envía el nombre de {string}")
    public void elUsuarioEnviaElNombreDe(String country) {
        try{
            countryISOCodeModel.setCountryName(country);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("el usuario obtiene el código ISO {string}")
    public void elUsuarioObtieneElCodigoISO(String isoCode) {
        try{
            countryISOCodeModel.setIsoCode(isoCode);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: " + HttpStatus.SC_OK,
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la consulta debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    // Second Scenario.
    @When("el usuario envía el nombre de un país que no existe {string}")
    public void elUsuarioEnviaElNombreDeUnPaisQueNoExiste(String country) {
        try{
            countryISOCodeModel.setCountryName(country);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE)
                            .andTheHeaders(super.headers())
                            .andTheBodyRequest(bodyRequest())
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("el usuario obtiene el mensaje {string}")
    public void elUsuarioObtieneElMensaje(String message) {
        try{
            countryISOCodeModel.setIsoCode(message);
            actor.should(
                    seeThatResponse("El código de respuesta HTTP debe ser: " + HttpStatus.SC_OK,
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("El resultado de la consulta debe ser: ",
                            resturnSoapServiceResponse(),
                            containsString(bodyResponse()))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    private CountryISOCodeModel countryISOCodeModel(){
        return countryISOCodeModel;
    }

    private String bodyRequest(){
        return String.format(readFile(COUNTRY_ISO_CODE.getValue()), countryISOCodeModel().getCountryName());
    }

    private String bodyResponse(){
        return String.format(COUNTRY_ISO_CODE_RESPONSE.getValue(), countryISOCodeModel().getIsoCode());
    }
}
