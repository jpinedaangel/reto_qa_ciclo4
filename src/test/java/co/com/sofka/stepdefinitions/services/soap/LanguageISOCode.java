package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.WebSamplesModels;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.languageisocode.Patch.LANGUAGE_ISO_CODE_DV;
import static co.com.sofka.util.services.soap.languageisocode.Response.LANGUAGE_ISO_CODE_DV_RESPONSE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class LanguageISOCode extends WebSamplesSetup {
   private static final Logger LOGGER = Logger.getLogger(CountryCurrency.class);
   private WebSamplesModels searchLanguageISOCode;

   //Scenario1
   @Given("que el usuario quiera buscar el nombre del lenguaje {string}")
   public void queElUsuarioQuieraBuscarElNombreDelLenguaje(String language) {
      try {
         super.setupWebSamples();
         searchLanguageISOCode = new WebSamplesModels();
         searchLanguageISOCode.setLanguage(language);
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @When("el usuario hace la petición de busqueda el ISO Code del Lenguaje")
   public void elUsuarioHaceLaPeticionDeBusquedaElISOCodeDelLenguaje() {
      try {
         actor.attemptsTo(
            doPost()
               .withTheResource(RESOURCE)
               .andTheHeaders(super.headers())
               .andTheBodyRequest(bodyRequest())
         );

      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }
   @Then("el ususario debería obtener el ISO Code del Lenguaje {string}")
   public void elUsusarioDeberiaObtenerElISOCodeDelLenguaje(String answer) {
      try {
         searchLanguageISOCode.setAnswer(answer);
         actor.should(
            seeThatResponse("El código de rspuesta HTTP debe ser: ",
               response -> response.statusCode(HttpStatus.SC_OK)),
            seeThat("El resultado de la busqueda debe ser: ",
               resturnSoapServiceResponse(),
               containsString(bodyResponse()))
         );
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }


   //Scenario2
   @Given("que el usuario quiera buscar el nombre del lenguaje que no concuerda {string}")
   public void queElUsuarioQuieraBuscarElNombreDelLenguajeQueNoConcuerda(String wronglanguage) {
      try {
         super.setupWebSamples();
         searchLanguageISOCode = new WebSamplesModels();
         searchLanguageISOCode.setLanguage(wronglanguage);
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @When("el usuario hace la petición de busqueda de el ISO Code del Lenguaje")
   public void elUsuarioHaceLaPeticionDeBusquedaDeElISOCodeDelLenguaje() {
      try {
         actor.attemptsTo(
            doPost()
               .withTheResource(RESOURCE)
               .andTheHeaders(super.headers())
               .andTheBodyRequest(bodyRequest())
         );

      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @Then("el ususario debería obtener como resultado de la busqueda {string}")
   public void elUsusarioDeberiaObtenerComoResultadoDeLaBusqueda(String answer) {
      try {
         searchLanguageISOCode.setAnswer(answer);
         actor.should(
            seeThatResponse("El código de rspuesta HTTP debe ser: ",
               response -> response.statusCode(HttpStatus.SC_OK)),
            seeThat("El resultado de la busqueda debe ser: ",
               resturnSoapServiceResponse(),
               containsString(bodyResponse()))
         );
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   private WebSamplesModels searchLanguageISOCode(){
      return searchLanguageISOCode;
   }

   private String bodyRequest(){
      return String.format(readFile(LANGUAGE_ISO_CODE_DV.getValue()), searchLanguageISOCode().getLanguage());
   }

   private String bodyResponse(){
      return String.format(LANGUAGE_ISO_CODE_DV_RESPONSE.getValue(), searchLanguageISOCode().getAnswer());
   }


}
