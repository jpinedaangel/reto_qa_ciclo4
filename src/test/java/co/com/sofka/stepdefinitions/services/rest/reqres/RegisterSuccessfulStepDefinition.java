package co.com.sofka.stepdefinitions.services.rest.reqres;

import co.com.sofka.ReqresSetup;
import co.com.sofka.model.services.rest.reqres.registersuccessful.Response;
import co.com.sofka.model.services.rest.reqres.registersuccessful.User;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.model.services.rest.reqres.registersuccessful.ErrorMessage.ERROR;
import static co.com.sofka.question.services.rest.reqres.registersuccessful.ReturnRegisterJsonResponse.returnRegisterJsonResponse;
import static co.com.sofka.task.services.rest.DoPost.doPost;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public class RegisterSuccessfulStepDefinition extends ReqresSetup {
    private static final Logger LOGGER = Logger.getLogger(RegisterSuccessfulStepDefinition.class);
    private User user;

    @Given("un usuario permitido se encuentra en la página de registro")
    public void unUsuarioPermitidoSeEncuentraEnLaPaginaDeRegistro() {
        try {
            super.setupReqres();
            user = new User();
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @When("el usuario envía una peticion de regitro con el email {string} y el password {string}")
    public void elUsuarioEnviaUnaPeticionDeRegitroConElEmailYElPassword(String email, String password) {
        try {
            user.setEmail(email);
            user.setPassword(password);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE_REGISTER)
                            .andTheBodyRequest(user)
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("el usuario ve un código de respuesta exitoso con su id y su token")
    public void elUsuarioVeUnCodigoDeRespuestaExitosoConSuIdYSuToken() {
        try {
            Response actualResponse = returnRegisterJsonResponse().answeredBy(actor);
            actor.should(
                    seeThatResponse("El código de respuesta es: " + HttpStatus.SC_OK,
                            response -> response.statusCode(HttpStatus.SC_OK)),
                    seeThat("Retorna información",
                            act -> actualResponse, notNullValue())
            );
            actor.should(
                    seeThat("El id generado es 4",
                            act -> actualResponse.getId(), equalTo(4)),
                    seeThat("Se genera un toquen",
                            act -> actualResponse.getToken(), notNullValue())
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    // Second Scenario
    @Given("un usuario no permitido del website intenta registrarse")
    public void unUsuarioNoPermitidoDelWebsiteIntentaRegistrarse() {
        try {
            super.setupReqres();
            user = new User();
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @When("el usuario inteta una petición de registro con el email {string} y el password {string}")
    public void elUsuarioIntetaUnaPeticionDeRegistroConElEmailYElPassword(String email, String password) {
        try {
            user.setEmail(email);
            user.setPassword(password);
            actor.attemptsTo(
                    doPost()
                            .withTheResource(RESOURCE_REGISTER)
                            .andTheBodyRequest(user)
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("el usuario ve un código bad request y un mensaje de error")
    public void elUsuarioVeUnCodigoBadRequestYUnMensajeDeError() {
        try {
            Response actualResponse = returnRegisterJsonResponse().answeredBy(actor);
            actor.should(
                    seeThatResponse("El código de respuesta es: " + HttpStatus.SC_BAD_REQUEST,
                            response -> response.statusCode(HttpStatus.SC_BAD_REQUEST)),
                    seeThat("Retorna información",
                            act -> actualResponse, notNullValue())
            );
            actor.should(
                    seeThat("Se genera un error",
                            act -> actualResponse.getError(), equalTo(ERROR.getValue()))
            );
        } catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

}
