package co.com.sofka.stepdefinitions.services.soap;

import co.com.sofka.WebSamplesSetup;
import co.com.sofka.model.services.soap.WebSamplesModels;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import static co.com.sofka.question.services.soap.ResturnSoapServiceResponse.resturnSoapServiceResponse;
import static co.com.sofka.task.services.soap.DoPost.doPost;
import static co.com.sofka.util.FileUtilities.readFile;
import static co.com.sofka.util.services.soap.countrycurrency.Patch.COUNTRY_CURRENCY_DV;
import static co.com.sofka.util.services.soap.countrycurrency.Response.COUNTRY_CURRENCY_DV_RESPONSE1;
import static co.com.sofka.util.services.soap.countrycurrency.Response.COUNTRY_CURRENCY_DV_RESPONSE2;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class CountryCurrency extends WebSamplesSetup {

   private static final Logger LOGGER = Logger.getLogger(CountryCurrency.class);
   private WebSamplesModels searchCurrency;
   private WebSamplesModels searchCurrencyName;
   private static final boolean TRUE = true;


   //Scenario1
   @Given("que el usuario quiera buscar el codigo {string}")
   public void queElUsuarioQuieraBuscarElCodigo(String isoCode) {
      try {
         super.setupWebSamples();
         searchCurrency = new WebSamplesModels();
         searchCurrency.setCode(isoCode);
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @When("el usuario hace la petición de busqueda la divisa requerida")
   public void elUsuarioHaceLaPeticionDeBusquedaLaDivisaRequerida() {
      try {
         actor.attemptsTo(
            doPost()
               .withTheResource(RESOURCE)
               .andTheHeaders(super.headers())
               .andTheBodyRequest(bodyRequest(TRUE))
         );
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @Then("el ususario debería obtener como iso divisa {string}")
   public void elUsusarioDeberiaObtenerComoIsoDivisa(String answer) {
      try {
         searchCurrency.setAnswer(answer);
         actor.should(
            seeThatResponse("El código de rspuesta HTTP debe ser: ",
               response -> response.statusCode(HttpStatus.SC_OK)),
            seeThat("El resultado de la busqueda debe ser: ",
               resturnSoapServiceResponse(),
               containsString(bodyResponse()))
         );
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   //Scenario2
   @Given("que el usuario quiera buscar el nombre de la divisa por medio del codigo {string}")
   public void queElUsuarioQuieraBuscarElNombreDeLaDivisaPorMedioDelCodigo(String isoCode) {
      try {
         super.setupWebSamples();
         searchCurrencyName = new WebSamplesModels();
         searchCurrencyName.setCode(isoCode);
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @When("el usuario hace la petición de busqueda de la divisa")
   public void elUsuarioHaceLaPeticionDeBusquedaDeLaDivisa() {
      try {
         actor.attemptsTo(
            doPost()
               .withTheResource(RESOURCE)
               .andTheHeaders(super.headers())
               .andTheBodyRequest(bodyRequest(!TRUE))
         );
      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   @Then("el ususario debería obtener como resultado el valor {string}")
   public void elUsusarioDeberiaObtenerComoResultadoElValor(String answer) {
      try {
         searchCurrencyName.setAnswer(answer);
         actor.should(
            seeThatResponse("El código de rspuesta HTTP debe ser: ",
               response -> response.statusCode(HttpStatus.SC_OK)),
            seeThat("El resultado de la busqueda debe ser: ",
               resturnSoapServiceResponse(),
               containsString(bodyResponse2()))
         );

      }catch (Exception e){
         LOGGER.warn(e.getMessage());
      }
   }

   private WebSamplesModels searchCurrency(){
      return searchCurrency;
   }

   private WebSamplesModels searchCurrencyName(){
      return searchCurrencyName;
   }

   private String bodyRequest(boolean condition){
      if (condition){
         return String.format(readFile(COUNTRY_CURRENCY_DV.getValue()), searchCurrency().getCode());
      }else {
         return String.format(readFile(COUNTRY_CURRENCY_DV.getValue()), searchCurrencyName().getCode());
      }
   }

   private String bodyResponse(){
      return String.format(COUNTRY_CURRENCY_DV_RESPONSE1.getValue(), searchCurrency().getAnswer());
   }

   private String bodyResponse2(){
      return String.format(COUNTRY_CURRENCY_DV_RESPONSE2.getValue(), searchCurrencyName().getAnswer());
   }



}
