package co.com.sofka.stepdefinitions.services.rest.reqres;

import co.com.sofka.ReqresSetup;
import co.com.sofka.model.services.rest.PatchModel;
import co.com.sofka.question.services.rest.ResponseCode;
import co.com.sofka.question.services.rest.ResponseQuestionPatch;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.task.services.rest.DoPatch.doPatch;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class PatchStepDEfinition extends ReqresSetup {
    public static Logger LOGGER = Logger.getLogger(PatchStepDEfinition.class);
    private PatchModel patchModel;
    private final String NUMBER_USER="2";

    @Given("el usuario está en la página Reqres ingresa el nombre {string} y el campo trabajo {string}")
    public void elUsuarioEstaEnLaPaginaReqresIngresaElNombreYElCampoTrabajo (String nombre, String trabajo) {
        try {
            super.setupReqres();
            patchModel = new PatchModel();
            patchModel.setName(nombre);
            patchModel.setJob(trabajo);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }


    @When("cuando el usuario hace una petición de actualizacion de registro")
    public void cuandoElUsuarioHaceUnaPeticionDeActualizacionDeRegistro () {
        try {
            actor.attemptsTo(
                    doPatch()
                            .withTheResource(String.format(RESOURCE_USERS, NUMBER_USER))
                            .andTheBodyRequest(bodyRequestReqres())
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    @Then("el usuario deberá ver un codigo de respuesta {int} y los datos creados")
    public void elUsuarioDeberaVerUnCodigoDeRespuestaYLosDatosCreados (Integer status) {
        try {
            actor.should(
                    seeThat("El codigo de respuesta", ResponseCode
                            .was(), equalTo(status)),
                    seeThat("el en el campo trabajo debe aparecer ",
                            datos -> new ResponseQuestionPatch().answeredBy(actor).getJob(),
                            equalTo("zion resident"))
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }


    }

    @Given("el usuario está en la página de actualizacion  de Reqres")
    public void elUsuarioEstaEnLaPaginaDeActualizacionDeReqres () {
        try {
            super.setupReqres();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    @When("cuando el usuario hace una petición de actualizacion vacia")
    public void cuandoElUsuarioHaceUnaPeticionDeActualizacionVacia () {
        try {
            actor.attemptsTo(
                    doPatch()
                            .withTheResource(String.format(RESOURCE_USERS, NUMBER_USER))
                            .andTheBodyRequest("")
            );
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    @Then("el usuario deberá ver unicamente un codigo {int}")
    public void elUsuarioDeberaVerUnicamenteUnCodigo (Integer status) {
        try {
            actor.should(
                    seeThat("El codigo de respuesta", ResponseCode
                            .was(), equalTo(status)));
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            Assertions.fail("");

        }

    }

    private String bodyRequestReqres () {
        return "{\n" +
                "    \"name\": \"" + patchModel.getName() + "\",\n" +
                "    \"job\": \"" + patchModel.getJob() + "\"\n" +
                "}";
    }
}
