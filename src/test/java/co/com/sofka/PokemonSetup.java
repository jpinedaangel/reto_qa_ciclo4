package co.com.sofka;

import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

public class PokemonSetup extends GeneralSetup {
    protected static final String URL_BASE_POKEMON = "https://pokeapi.co";
    protected static final String RESOURCE_POKEMON = "/api/v2/pokemon?limit=10&offset=141";

    protected void setUpPokemon() {
        actorCanPokemon();
    }
    private void actorCanPokemon(){
        actor.can(CallAnApi.at(URL_BASE_POKEMON));
    }

}
