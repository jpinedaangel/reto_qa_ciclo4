package co.com.sofka;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import java.util.HashMap;

public class GeneralSetup {
    protected static final String URL_BASE_WEB_EXAMPLE = "http://webservices.oorsprong.org";
    protected static final String RESOURCE_WEB_EXAMPLE = "/websamples.countryinfo/CountryInfoService.wso";
    public final Actor actor = new Actor("Ivan");

    protected void actorCan (String URL_BASE) {
        actor.can(CallAnApi.at(URL_BASE));
    }

    protected HashMap<String, Object> headers() {
        HashMap<String, Object> headersCollections = new HashMap<>();
        headersCollections.put("Content-Type", "text/xml;charset=UTF-8");
        headersCollections.put("SOAPAction", "");
        return headersCollections;
    }

    protected HashMap<String, Object> headers(String value) {
        HashMap<String, Object> headersCollections = new HashMap<>();
        headersCollections.put("Content-Type", "text/xml;charset=UTF-8");
        headersCollections.put("SOAPAction", value);
        return headersCollections;
    }
}
