package co.com.sofka;

public class TempConvertSetup extends GeneralSetup{
    protected static final String URL_BASE = "https://www.w3schools.com";
    protected static final String RESOURCE = "/xml/tempconvert.asmx?wsdl";
    protected static final String HEADER_CTF="https://www.w3schools.com/xml/CelsiusToFahrenheit";
    protected static final String HEADER_FTC="https://www.w3schools.com/xml/FahrenheitToCelsius";

    protected void setupTempConvert() {
        actorCan(URL_BASE);
    }


}
