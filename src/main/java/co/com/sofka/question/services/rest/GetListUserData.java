package co.com.sofka.question.services.rest;

import co.com.sofka.model.services.rest.UserListData;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;

public class GetListUserData implements Question<List<UserListData>> {

    public static Question<List<UserListData>> inTheResponse(){return new GetListUserData();}

    @Override
    public List<UserListData> answeredBy(Actor actor) {
        return SerenityRest.lastResponse().jsonPath().getList("data", UserListData.class);
    }
}
