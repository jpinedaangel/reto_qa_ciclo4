package co.com.sofka.question.services.rest;

import co.com.sofka.model.services.rest.PatchModel;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ResponseQuestionPatch implements Question<PatchModel> {
    @Override
    public PatchModel answeredBy (Actor actor) {
        return SerenityRest.lastResponse().as(PatchModel.class);
    }
}
