package co.com.sofka.question.services.rest;

import co.com.sofka.model.services.rest.Pokedex;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ResponseQuestionPokemon implements Question <Pokedex>{

    @Override
    public Pokedex answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(Pokedex.class);
    }

}
