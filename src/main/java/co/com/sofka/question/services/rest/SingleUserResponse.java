package co.com.sofka.question.services.rest;

import co.com.sofka.model.services.rest.singleuser.User;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class SingleUserResponse implements Question<User> {

    @Override
    public User answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(User.class);
    }

    public static SingleUserResponse singleUserResponse(){
        return new SingleUserResponse();
    }
}
