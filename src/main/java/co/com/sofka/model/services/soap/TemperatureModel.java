package co.com.sofka.model.services.soap;

public class TemperatureModel {

    private String celsiusTemperature;
    private String fahrenheitTemperature;

    public String getCelsiusTemperature() {
        return celsiusTemperature;
    }

    public void setCelsiusTemperature(String celsiusTemperature) {
        this.celsiusTemperature = celsiusTemperature;
    }

    public String getFahrenheitTemperature() {
        return fahrenheitTemperature;
    }

    public void setFahrenheitTemperature(String fahrenheitTemperature) {
        this.fahrenheitTemperature = fahrenheitTemperature;
    }
}
