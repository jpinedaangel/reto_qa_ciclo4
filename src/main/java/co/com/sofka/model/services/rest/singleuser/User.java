package co.com.sofka.model.services.rest.singleuser;

import co.com.sofka.model.services.rest.UserListData;

public class User {
    private UserListData data;
    private Support support;
    private String email;

    public UserListData getData() {
        return data;
    }

    public void setData(UserListData data) {
        this.data = data;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}