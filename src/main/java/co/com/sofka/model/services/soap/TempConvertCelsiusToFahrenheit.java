package co.com.sofka.model.services.soap;

public class TempConvertCelsiusToFahrenheit {

    private int a;
    private int outcome;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getOutcome() {
        return outcome;
    }

    public void setOutcome(int outcome) {
        this.outcome = outcome;
    }

}