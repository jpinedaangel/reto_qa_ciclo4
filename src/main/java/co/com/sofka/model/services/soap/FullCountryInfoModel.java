package co.com.sofka.model.services.soap;

public class FullCountryInfoModel {

    private String codeA;
    private String codeB;
    private String capital;
    private String codeTele;

    public String getCodeA() {
        return codeA;
    }

    public void setCodeA(String codeA) {
        this.codeA = codeA;
    }

    public String getCodeB() {
        return codeB;
    }

    public void setCodeB(String codeB) {
        this.codeB = codeB;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getCodeTele() {
        return codeTele;
    }

    public void setCodeTele(String codeTele) {
        this.codeTele = codeTele;
    }
}
