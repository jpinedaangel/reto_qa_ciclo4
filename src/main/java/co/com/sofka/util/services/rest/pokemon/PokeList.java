package co.com.sofka.util.services.rest.pokemon;

public enum PokeList {
    AERODACTYL("aerodactyl"),
    SNORLAX("snorlax"),
    ARTICUNO("articuno"),
    ZAPDOS("zapdos"),
    MOLTRES("moltres"),
    DRATINI("dratini"),
    DRAGONAIR("dragonair"),
    DRAGONITE("dragonite"),
    MEWTWO("mewtwo"),
    MEW("mew");


    private final String value;

    public String getValue() {
        return value;
    }

    PokeList(String value) {
        this.value = value;
    }

}
