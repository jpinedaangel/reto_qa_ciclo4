package co.com.sofka.util.services.soap.languajename;

public enum ResponseLanguajeName {
    SEARCH_RESPONSE("<m:LanguageNameResult>%s</m:LanguageNameResult>");

    private final String value;

    ResponseLanguajeName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
