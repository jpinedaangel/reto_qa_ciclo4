package co.com.sofka.util.services.soap.countryname;

import static co.com.sofka.util.Utilities.*;

public enum Patch {

    COUNTRYNAME(osPathModify(defineOS(), getUserDir()
            + "\\src\\test\\resources\\files\\services\\soap\\websamples\\countryname.xml"));

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
