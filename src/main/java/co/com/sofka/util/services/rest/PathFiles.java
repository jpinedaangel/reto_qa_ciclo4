package co.com.sofka.util.services.rest;
import static co.com.sofka.util.Utilities.*;
public enum PathFiles {

    ROOT_REQRES("src/test/resources/files/services/rest/reqres/responses/"),

    LIST_RESOURCES(ROOT_REQRES.getValue()+"resources.json");

    private final String value;

    PathFiles(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
