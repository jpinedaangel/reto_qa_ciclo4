package co.com.sofka.util.services.soap.currencyname;

public enum Response {
    CURRENCY_RESPONSE("<m:CurrencyNameResult>%s</m:CurrencyNameResult>");

    private final String value;

    Response(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
