package co.com.sofka.util.services.soap;

public enum Responses {
    CAPITAL_CITY_RESPONSE("<m:CapitalCityResult>%s</m:CapitalCityResult>"),
    LANGUAGE_NAME_RESPONSE("<m:LanguageNameResult>%s</m:LanguageNameResult>"),
    ERROR_SERVER_RESPONSE("Server Error"),
    TEMP_CONVERT_RESPONSE("<FahrenheitToCelsiusResult>%s</FahrenheitToCelsiusResult>");


    private final String value;

    Responses(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
