package co.com.sofka.util.services.soap.intphonecountry;

public enum ResponseIntPhone {
    SEARCH_RESPONSE_INT_PHONE("<m:CountryIntPhoneCodeResult>%s</m:CountryIntPhoneCodeResult>");

    private final String value;

    ResponseIntPhone(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
