package co.com.sofka.util.services.soap.languageisocode;

public enum Patch {
    LANGUAGE_ISO_CODE_DV("src/test/resources/files/services/soap/websamples/LanguageISOCode.xml");

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
