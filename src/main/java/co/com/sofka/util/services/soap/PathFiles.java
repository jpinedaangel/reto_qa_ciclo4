package co.com.sofka.util.services.soap;


import static co.com.sofka.util.Utilities.*;

public enum PathFiles {
    ROOT_WEB_SAMPLE_FILES(getUserDir()
    +osPathModify(defineOS(),"\\src\\test\\resources\\files\\services\\soap\\websamples\\request\\")),

    CAPITAL_CITY_REQUEST(ROOT_WEB_SAMPLE_FILES.getValue()+"capitalcity.xml");


    private final String value;

    PathFiles(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
