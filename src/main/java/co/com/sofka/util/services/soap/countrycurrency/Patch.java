package co.com.sofka.util.services.soap.countrycurrency;

public enum Patch {
    COUNTRY_CURRENCY_DV("src/test/resources/files/services/soap/websamples/CountryCurrency.xml");

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
