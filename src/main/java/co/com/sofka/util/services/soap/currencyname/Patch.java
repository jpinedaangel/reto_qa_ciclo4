package co.com.sofka.util.services.soap.currencyname;

public enum Patch {
    CURRENCY_NAME( "src/test/resources/files/services/soap/websamples/CurrencyName.xml");

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
