package co.com.sofka.util.services.soap.languageisocode;

public enum Response {
  LANGUAGE_ISO_CODE_DV_RESPONSE("<m:LanguageISOCodeResult>%s</m:LanguageISOCodeResult>");

    private final String value;

    Response(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
