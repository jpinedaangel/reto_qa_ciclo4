package co.com.sofka.util.services.soap.listofcontinentsbyname;

public enum ResponseListOfContinentsByName {
    RESPONSE_LIST_OF_CONTINENTS_BY_NAME("<m:sName>%s</m:sName>");

    private final String value;


    ResponseListOfContinentsByName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
