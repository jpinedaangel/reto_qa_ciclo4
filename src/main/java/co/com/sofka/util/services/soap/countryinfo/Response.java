package co.com.sofka.util.services.soap.countryinfo;

public enum Response {
    COUNTRY_ISO_CODE_RESPONSE("<m:CountryISOCodeResult>%s</m:CountryISOCodeResult>");

    private final String value;

    Response(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
