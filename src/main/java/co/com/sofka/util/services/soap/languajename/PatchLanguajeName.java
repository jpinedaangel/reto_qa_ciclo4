package co.com.sofka.util.services.soap.languajename;


public enum PatchLanguajeName {

    BUSCAR("src/test/resources/files/services/soap/websamples/LanguajeName.xml");

    private final String value;

    PatchLanguajeName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
