package co.com.sofka.util.services.soap.celsiustofahrenheit;

public enum Response {

    CONVERT_RESPONSE("<CelsiusToFahrenheitResult>%s</CelsiusToFahrenheitResult>");

    private final String value;

    Response(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
