package co.com.sofka.util.services.soap.countryflag;

import static co.com.sofka.util.Utilities.*;

public enum Patch {

    COUNTRYFLAG(osPathModify(defineOS(), getUserDir()
            + "\\src\\test\\resources\\files\\services\\soap\\websamples\\countryflag.xml"));

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
