package co.com.sofka.util.services.soap.countryflag;

public enum Response {

    COUNTRYFLAG_RESPONSE("<m:CountryFlagResult>%s</m:CountryFlagResult>");

    private final String value;

    Response(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
