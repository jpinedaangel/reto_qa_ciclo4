package co.com.sofka.util.services.soap.intphonecountry;


public enum PatchIntPhone {
    BUSCAR_INT_PHONE( "src/test/resources/files/services/soap/websamples/IntPhoneCountry.xml");

    private final String value;

    PatchIntPhone(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
