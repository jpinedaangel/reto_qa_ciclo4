package co.com.sofka.util.services.soap.celsiustofahrenheit;

public enum Patch {

    CONVERT("src/test/resources/files/services/soap/tempconvert/celsiusToFahrenheit.xml");

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
