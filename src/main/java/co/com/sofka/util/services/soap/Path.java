package co.com.sofka.util.services.soap;

public enum Path {

    TEMP_CONVERT_PATH("src/test/resources/files/services/soap/tempconvert/convertToFahrenheit.xml");

    private final String value;

    Path(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
