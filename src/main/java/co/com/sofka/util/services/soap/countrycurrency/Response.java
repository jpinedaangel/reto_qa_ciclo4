package co.com.sofka.util.services.soap.countrycurrency;

public enum Response {
    COUNTRY_CURRENCY_DV_RESPONSE1("<m:sISOCode>%s</m:sISOCode>"),
    COUNTRY_CURRENCY_DV_RESPONSE2("<m:sName>%s</m:sName>");


    private final String value;

    Response(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
