package co.com.sofka.util.services.soap.countryinfo;

import static co.com.sofka.util.Utilities.*;

public enum Path {
    COUNTRY_ISO_CODE(osPathModify(defineOS(), getUserDir() +
            "/src/test/resources/files/services/soap/websamples/CountryIsoCode.xml"));

    private final String value;

    Path(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
