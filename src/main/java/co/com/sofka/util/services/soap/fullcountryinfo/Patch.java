package co.com.sofka.util.services.soap.fullcountryinfo;

public enum Patch {
    FULLCOUNTRYINFO("src/test/resources/files/services/soap/fullcountryinfo/fullCountryInfo.xml");

    private final String value;

    Patch(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
