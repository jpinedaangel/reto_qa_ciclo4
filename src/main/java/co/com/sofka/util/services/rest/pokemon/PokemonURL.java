package co.com.sofka.util.services.rest.pokemon;

public enum PokemonURL {
    AERODACTYL("https://pokeapi.co/api/v2/pokemon/142/"),
    SNORLAX("https://pokeapi.co/api/v2/pokemon/143/"),
    ARTICUNO("https://pokeapi.co/api/v2/pokemon/144/"),
    ZAPDOS("https://pokeapi.co/api/v2/pokemon/145/"),
    MOLTRES("https://pokeapi.co/api/v2/pokemon/146/"),
    DRATINI("https://pokeapi.co/api/v2/pokemon/147/"),
    DRAGONAIR("https://pokeapi.co/api/v2/pokemon/148/"),
    DRAGONITE("https://pokeapi.co/api/v2/pokemon/149/"),
    MEWTWO("https://pokeapi.co/api/v2/pokemon/150/"),
    MEW("https://pokeapi.co/api/v2/pokemon/151/");

    private final String value;

    public String getValue() {
        return value;
    }

    PokemonURL(String value) {
        this.value = value;
    }
}
