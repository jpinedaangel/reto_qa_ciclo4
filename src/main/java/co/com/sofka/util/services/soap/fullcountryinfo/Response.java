package co.com.sofka.util.services.soap.fullcountryinfo;

public enum Response {

    FULLCOUNTRYINFO_C_RESPONSE("<m:sCapitalCity>%s</m:sCapitalCity>"),
    FULLCOUNTRYINFO_NUM_RESPONSE("<m:sPhoneCode>%s</m:sPhoneCode>");


    private final String value;

    Response(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
