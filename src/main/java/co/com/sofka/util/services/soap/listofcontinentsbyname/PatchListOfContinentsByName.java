package co.com.sofka.util.services.soap.listofcontinentsbyname;

public enum PatchListOfContinentsByName {
    LIST_OF_CONTINENTS_BY_NAME("src/test/resources/files/services/soap/websamples/ListOfContinentsByName.xml");
    private final String value;

    PatchListOfContinentsByName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
