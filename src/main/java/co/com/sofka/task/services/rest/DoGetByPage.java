package co.com.sofka.task.services.rest;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class DoGetByPage implements Task {

    private String resource;
    private int pathParam;

    public DoGetByPage withTheResource(String resource){
        this.resource = resource;
        return this;
    }

    public DoGetByPage withThePathParam(int pathParam){
        this.pathParam = pathParam;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Get.resource(resource)
                        .with(request -> request.pathParam("page",pathParam))
        );
    }

    public static DoGetByPage doGet(){
        return new DoGetByPage();
    }
}
