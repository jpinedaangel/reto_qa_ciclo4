package co.com.sofka.task.services.rest;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

import java.util.Map;

public class DoGetFull implements Task {
    private String resource;
    private Map<String, Object> headers;
    private String bodyRequest;

    public DoGetFull withTheResource (String resource){
        this.resource=resource;
        return this;
    }
    public DoGetFull andTheHeaders(Map<String, Object> headers) {
        this.headers = headers;
        return this;
    }
    public DoGetFull andTheBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Get.resource(resource)
                        .with(requestSpecification->requestSpecification.relaxedHTTPSValidation()
                                .headers(headers)
                                .body(bodyRequest))
        );
    }

    public static DoGetFull doGet(){return new DoGetFull();}
}
